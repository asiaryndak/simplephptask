<?php
include "News.php";

$data = file_get_contents('php://input');

$decodeData = json_decode($data);
$id=$decodeData->{'editNewsId'};
$newTitle=$decodeData->{'title'};
$newContent=$decodeData->{'content'};
$newCategory_id=$decodeData->{'category_id'};

//if ($_POST['editNewsId']) {
/*    $id = $_POST['editNewsId'];
    $newTitle = $_POST['title'];
    $newContent = $_POST['content'];
    $newCategory_id = $_POST['category_id'];*/


    $news = News::getOneNews($id);
    /*Jeśli którakolwiek ze zmiennych do edycji przychodzi pusta, wówczas do metody edytowania wysyłane są jej stare dane*/
    if ($news != null) {
        if ($newTitle == null) {
            $title = $news['title'];
        } else {
            $title = $newTitle;
        }

        if ($newContent == null) {
            $content = $news['content'];
        } else {
            $content = $newContent;
        }

        if ($newCategory_id == null) {
            $category_id = $news['category_id'];
        } else {
            $category_id = $newCategory_id;
        }

        News::updateNews(htmlentities($id), htmlentities($title), htmlentities($content), htmlentities($category_id));
        $updatedNews = News::getOneNews($id);
        $categoryName = News::getCategoryName($updatedNews['category_id']);
        $updateNews = ['id' => $id, 'title' => $updatedNews['title'], 'content' => $updatedNews['content'], 'category' => $categoryName['name']];
        echo json_encode($updateNews);
    } else {
        echo "Nie znaleziono takiego rekordu w bazie danych";
    }
//}
?>