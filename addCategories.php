<?php
include "News.php";

$categoryName = $_POST['categoryName'];

$newCategoryId = News::addCategory($categoryName);
$newCategory = News::getCategoryName($newCategoryId);

$newCategoryJSON = ["id"=>$newCategory['id'],"category_name"=>$newCategory['name']];
echo json_encode($newCategoryJSON);
?>