$(document).on('click', '.update', function () {
    let tr = $(this).closest('tr');
    let updatedNewsId = tr.attr('id');
    $title = document.getElementById("formTitle");
    $title.innerHTML="EDYTUJ NEWSA";
    $.ajax({
        type: 'POST',
        url: 'getOneNews.php',
        dataType: 'JSON',
        data: {newsId: updatedNewsId},
        success: function (response) {
            $("#submit_ajax_add_news_form").attr('value', 'Edytuj artykuł');
            $("#ajax_add_news_form").attr('onsubmit', 'update()');
            $("#cancel").show();

            window.location.href = "#ajax_add_news_form";

            $('#editNewsId').val(response.id);
            $('#title').val(response.title);
            $('#content').val(response.content);
            $('#category_id').val(response.category);
            // document.getElementById('#category_id').value = response.category;
        }
    });
});