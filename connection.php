<?php

class Database
{
    private static $conn = null;
    private static $dbName="news";           //database name
    private static $dbUsername= "root";         //username
    private static $dbUserPassword="";   // database user password
    private static $serverName= "localhost";    // server name
    public function __construct()
    {
        die('Init function is not allowed');
    }

    public static function connect()
    {
        if (null == self::$conn) {
            try {
//                self::$conn =  new PDO( "mysql:host=".self::$serverName.";"."dbname=".self::$dbName.";".self::$dbUsername, self::$dbUserPassword, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER_SET utf8_unicode_ci"));
                self::$conn =  new PDO( "mysql:host=".self::$serverName.";"."dbname=".self::$dbName.";".'charset = utf8',self::$dbUsername, self::$dbUserPassword);
//                self::$conn =  new PDO( "mysql:host=".self::$serverName.";"."dbname=".self::$dbName.";".self::$dbUsername, self::$dbUserPassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",PDO::MYSQL_ATTR_INIT_COMMAND =>'SET CHARACTER_SET utf8_unicode_ci'));
/*                $conn -> query ('SET NAMES utf8');
                $conn -> query ('SET CHARACTER_SET utf8_unicode_ci');*/
            } catch
            (PDOException $e) {
                die($e->getMessage());
            }
     /*       $conn = self::$conn;
            $conn -> query ('SET NAMES utf8');
            $conn -> query ('SET CHARACTER_SET utf8_unicode_ci');*/
            return self::$conn;
        }
    }

    public static function disconnect(){
        self::$conn=null;
    }

}
?>