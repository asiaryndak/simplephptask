<?php
require_once "News.php";

$data = file_get_contents('php://input');

$decodeData = json_decode($data);
$id=$decodeData->{'id'};

$news = News::getNews($id);
if (!($news == null)) {
    try {
        News::deleteNews($id);
        echo "Rekord został usunięty";
    } catch (Exception $e) {
        echo "Wystąpił błąd podczas usuwania";
    }
}else{
    echo "Nie znaleziono takiego rekordu w bazie danych";
}
?>