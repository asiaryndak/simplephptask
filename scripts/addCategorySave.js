$(document).ready(function () {
    $(document).on('click', '.categorySave', function() {

                $.ajax({
                    type: 'POST',
                    url: 'addCategories.php',
                    dataType: 'JSON',
                    data: {categoryName: $("#addCategoryName").val()},
                    success: function (response) {

                        let id = value['id'];
                        let categoryName = value['category_name'];

                        $("#category_id").append($('<option>', {
                            value: id,
                            text: categoryName
                        }));

                        $("#addCategoryName").prop('disabled', true);
                        $("#addCategoryName").hide();
                    }
                });

        }
    );
});