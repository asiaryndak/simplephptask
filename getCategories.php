<?php
include "News.php";
$categories = News::getCategories();
$categoryList = array();
foreach ($categories as $category){
    $oneCategory = ['id'=>$category['id'],'category_name'=>$category['name']];
    array_push($categoryList,$oneCategory);
}
echo json_encode($categoryList);
?>