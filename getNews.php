<?php
require_once 'News.php';
$news = News::getAllNews();
/*function utf8ize($d)
{
    if (is_array($d))
        foreach ($d as $k => $v)
            $d[$k] = utf8ize($v);

    else if (is_object($d))
        foreach ($d as $k => $v)
            $d->$k = utf8ize($v);
    else
        return utf8_encode($d);
    return $d;
}*/
$newsList = array();
foreach ($news as $new) {
    $categoryName = News::getCategoryName($new['category_id']);

    $oneNews = ['id' => $new['id'],'title' => $new['title'], 'content' => $new['content'], 'category' => $categoryName['name']];
    array_push($newsList, $oneNews);
}
echo json_encode($newsList);
//echo json_encode(utf8ize($newsList));
?>