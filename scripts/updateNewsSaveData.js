function update() {
    let array = {
        editNewsId: $('#editNewsId').val(),
        title: $('#title').val(),
        content: $('#content').val(),
        category_id: $('#category_id').val()
    };
    let arrayJSON = JSON.stringify(array);
    $.ajax({
        type: 'PUT',
        url: 'update.php',
        dataType: 'JSON',
        data: arrayJSON,
        success: function (response) {
            $("#submit_ajax_add_news_form").attr('value', 'Dodaj artykuł');
            $("#ajax_add_news_form").attr('onsubmit', 'addNews()');
            document.getElementById("ajax_add_news_form").reset();

            let id = response.id;
            let title = response.title;
            let content = response.content;
            let category = response.category;

            $updateData =
                "<td>" + title + "</td>" +
                "<td>" + content + "</td>" +
                "<td>" + category + "</td>" +
                '<td><button type="button" class="delete">Usuń</button></td>' +
                '<td><button type="button" class="update">Edytuj</button></td>';


            window.location.href = "#query-results";
            $row = $("#allnewstable").find("tr#" + response.id).html($updateData);
            $row.fadeOut(0);
            $row.fadeIn(1000);

        }
    })
        .fail(function () {
            alert(1);
        });
}
