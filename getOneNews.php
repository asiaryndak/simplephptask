<?php
require_once 'News.php';
$news_id = htmlspecialchars($_POST['newsId']);


if (!isset($_POST['searchCat'])) {
    $news = News::getOneNews($news_id);
    if (!($news == null)) {
            $categoryName = News::getCategoryName($news['category_id']);
//            $oneNews = ['id' => $news['id'], 'title' => $news['title'], 'content' => $news['content'], 'category' => $news['category_id']];
        $oneNews = ['id'=>html_entity_decode($news['id']),'title' => html_entity_decode($news['title']), 'content' => html_entity_decode($news['content']), 'category' => html_entity_decode($news['category_id'])];

        echo json_encode($oneNews);
    } else {
        $error = "Nie ma newsa o takim identyfikatorze";
        $oneNews = ['id' => '0', 'title' => '0', 'content' => $error, 'category' => '0'];
        echo json_encode($oneNews);
    }
} else {
    $searchCategory = $_POST['searchCat'];
//$newsList = array();

    /*function utf8ize($d)
    {
        if (is_array($d))
            foreach ($d as $k => $v)
                $d[$k] = utf8ize($v);

        else if (is_object($d))
            foreach ($d as $k => $v)
                $d->$k = utf8ize($v);
        else
            return utf8_encode($d);
        return $d;
    }*/

    if ($searchCategory == 'id') {
        if (!(is_numeric($news_id))) {
            $error = "Wprowadzone znaki nie są wartościami numerycznymi";
            $oneNews = ['id' => '0', 'title' => '0', 'content' => $error, 'category' => '0'];
            $newsList = array();
            array_push($newsList, $oneNews);
            echo json_encode($newsList);
//        echo json_encode($oneNews);
        } else {
            $news = News::getNews($news_id);
            if (!($news == null)) {
                $newsList = array();
                foreach ($news as $new) {
                    $categoryName = News::getCategoryName($new['category_id']);
                    $oneNews = ['id' => $new['id'], 'title' => $new['title'], 'content' => $new['content'], 'category' => $categoryName['name']];
                    array_push($newsList, $oneNews);
                }
                echo json_encode($newsList);
//            echo json_encode(utf8ize($newsList));
            } else {
                $error = "Nie ma newsa o takim identyfikatorze";
                $oneNews = ['id' => '0', 'title' => '0', 'content' => $error, 'category' => '0'];
                $newsList = array();
                array_push($newsList, $oneNews);
                echo json_encode($newsList);
//            echo json_encode($oneNews);
            }
        }
    } elseif ($searchCategory == 'title') {
        $news = News::getOneNewsByTitle($news_id);

        if (!($news == null)) {
            $newsList = array();
            foreach ($news as $new) {
                $categoryName = News::getCategoryName($new['category_id']);
                $oneNews = ['id' => $new['id'], 'title' => $new['title'], 'content' => $new['content'], 'category' => $categoryName['name']];
                array_push($newsList, $oneNews);
            }
//        echo json_encode(utf8ize($newsList));
            echo json_encode($newsList);
        } else {
            $error = "Nie ma newsa o takim tytule";
            $oneNews = ['id' => '0', 'title' => '0', 'content' => $error, 'category' => '0'];
            $newsList = array();
            array_push($newsList, $oneNews);
            echo json_encode($newsList);
//        echo json_encode($oneNews);
        }

    } elseif ($searchCategory == 'content') {
        $news = News::getOneNewsByContent($news_id);

        if (!($news == null)) {
            $newsList = array();
            foreach ($news as $new) {
                $categoryName = News::getCategoryName($new['category_id']);
                $oneNews = ['id' => $new['id'], 'title' => $new['title'], 'content' => $new['content'], 'category' => $categoryName['name']];
                array_push($newsList, $oneNews);
            }
            echo json_encode($newsList);
//        echo json_encode(utf8ize($newsList));
        } else {
            $error = "Nie ma newsa o takiej treści";
            $oneNews = ['id' => '0', 'title' => '0', 'content' => $error, 'category' => '0'];
            $newsList = array();
            array_push($newsList, $oneNews);
            echo json_encode($newsList);
//        echo json_encode($oneNews);
        }

    }
}
?>