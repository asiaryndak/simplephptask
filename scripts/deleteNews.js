$(document).ready(function(){
    $(document).on('click', '.delete', function() {
        let tr = $(this).closest('tr');
        let deleteId = $(this).closest('tr').attr('id');
        let array = { id:deleteId};
        let arrayJSON = JSON.stringify(array);
        $.ajax({
            url: 'deleteNews.php',
            type: 'DELETE',
            data: arrayJSON,
            // data: { id:deleteId},
            success: function(response){
                // alert(response);
                tr.fadeOut(800, function(){
                    tr.remove();
                });

            }
        });

    });

});
