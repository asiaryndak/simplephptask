function addNews() {
    $.ajax({
        type: 'POST',
        url: 'saveNewsToDB.php',
        dataType: 'JSON',
        data: {
            title: $('#title').val(),
            content: $('#content').val(),
            category_id: $('#category_id').val()
        },
        success: function (response) {
            let id = response.id;
            document.getElementById("ajax_add_news_form").reset();
            let title = response.title;
            let content = response.content;
            let category = response.category;
            let tr_str = "<tr id= " + id + ">" +
                "<td>" + title + "</td>" +
                "<td>" + content + "</td>" +
                "<td>" + category + "</td>" +
                '<td><button type="button" class="delete">Usuń</button></td>' +
                '<td><button type="button" class="update">Edytuj</button></td>'
                + "</tr>";


            $("#allnewstable").append(tr_str);
            $row = $("#allnewstable").find("tr#"+response.id).fadeOut();
            window.location.href = "#query-results";
            $row.fadeIn(400);



        }
    });
}