$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: 'getNews.php',
        dataType: 'JSON',
        success: function (response) {
            $("#query-results").show();
            Object.keys(response).forEach(function (i) {
                let id = response[i].id;
                let title = response[i].title;
                let content = response[i].content;
                let category = response[i].category;

                let tr_str = "<tr id= " + id + ">" +
                    // "<td style='display: none'>" + id + "</td>" +
                    "<td>" + title + "</td>" +
                    "<td>" + content + "</td>" +
                    "<td>" + category + "</td>" +
                    '<td><button type="button" class="delete">Usuń</button></td>' +
                    '<td><button type="button" class="update">Edytuj</button></td>'

                    + "</tr>";

                $("#allnewstable").append(tr_str);
            })
        }
    })
});