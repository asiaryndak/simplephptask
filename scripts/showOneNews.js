function showOneNews() {
    $.ajax({
        type: 'POST',
        url: 'getOneNews.php',
        dataType: 'JSON',
        data: $("#searchForm").serialize(),
        success: function (response) {

            $('#searchnewstable').find('tr:gt(0)').remove();
            document.getElementById("searchForm").reset();
            $('#searcherrorstable').find('tr:first').remove();

            Object.keys(response).forEach(function (i) {

                let id = response[i].id;
                let title = response[i].title;
                let content = response[i].content;
                let category = response[i].category;

                if (id > 0) {
                    $("#search-results").show();
                    let tr_str = "<tr>" +
                        "<td>" + title + "</td>" +
                        "<td>" + content + "</td>" +
                        "<td>" + category + "</td>" +
                        +"</tr>";
                    $("#searchnewstable").append(tr_str);
                }

                else {
                    $("#search-results").hide();
                    $("#searchErrors").show();
                    let tr_str = "<tr style='background: #ec776b;height: 2em;text-align: center; text-transform: uppercase;'>" +
                        "<td></td>" +
                        "<td>" + content + "</td>" +
                        "<td></td>" +
                        +"</tr>";
                    $("#searcherrorstable").append(tr_str);
                }
            })
        }
    })
}