<?php

include 'connection.php';

class News
{

    public static function getOneNews($newsId)
    {
        $pdo = Database::connect();
//        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = $pdo->prepare("Select * from `news` where `id` = ?");
        $sql->execute(array($newsId));
        $news = $sql->fetch(PDO::FETCH_ASSOC);
//        $news = $sql->fetchAll();
        Database::disconnect();
        return $news;
    }


    public static function getNews($newsId)
    {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = $pdo->prepare("Select * from `news` where `id` = ?");
        $sql->execute(array($newsId));
        //$news = $sql->fetch(PDO::FETCH_ASSOC);
        $news = $sql->fetchAll();
        Database::disconnect();
        return $news;
    }

    public static function getOneNewsByTitle($title)
    {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = $pdo->prepare("Select * from `news` where `title` LIKE ?");
        $sql->execute(array("%$title%"));
        //$news = $sql->fetch(PDO::FETCH_ASSOC);
        $news = $sql->fetchAll();
        Database::disconnect();
        return $news;
    }

    public static function getOneNewsByContent($content)
    {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = $pdo->prepare("Select * from `news` where `content` LIKE ?");
        $sql->execute(array("%$content%"));
//        $news = $sql->fetch(PDO::FETCH_ASSOC);
        $news = $sql->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();
        return $news;
    }

    /*    public static function getNewsByCategory($categoryName){
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = $pdo->prepare("Select * from `categories` where `name` LIKE ?");
            $sql->execute(array("%$categoryName%"));
            $news = $sql->fetch(PDO::FETCH_ASSOC);
            Database::disconnect();
            return $news;
        }*/
    public static function getCategories()
    {
        $pdo = Database::connect();
        $sql = "select * from categories";
        $exec = $pdo->query($sql);

        Database::disconnect();
        return $exec;
    }

    public static function getCategoryName($categoryId)
    {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = $pdo->prepare("Select * from `categories` where `id` = ?");
        $sql->execute(array($categoryId));
        $category = $sql->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
        return $category;
    }

    public static function addCategory($categoryName){
        $pdo = Database::connect();
        $sql = "INSERT INTO categories(name) VALUES (?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($categoryName));
        $lastId = $pdo->lastInsertId();

        Database::disconnect();
        return $lastId;
    }

    public static function getAllNews()
    {
        $pdo = Database::connect();
        $sql = "select * from news";
        $exec = $pdo->query($sql);

        Database::disconnect();
        return $exec;
    }


    public static function createNews(string $title, string $content, int $categoryId)
    {

        $pdo = Database::connect();
        $sql = "INSERT INTO news(title,content,category_id) VALUES (?,?,?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($title, $content, $categoryId));
        $lastId = $pdo->lastInsertId();

        Database::disconnect();
        return $lastId;
    }

    public static function deleteNews($id)
    {
        $pdo = Database::connect();
        try {
            $sql = "DELETE FROM `news` where `id` = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($id));
            echo "Usunięto poprawnie";
        } catch (Exception $e) {
            echo "Błąd podczas usuwania";
        }
        Database::disconnect();
    }

    public static function updateNews($id, $title, $content, $category_id)
    {
        $pdo = Database::connect();
        $newsId = $id;
        $newsTitle = $title;
        $newsContent = $content;
        $newsCategoryId = $category_id;
        try {
            $sql = "UPDATE news SET title=?, content=?, category_id=? where id=?";
            $query = $pdo->prepare($sql);
            $query->execute(array($newsTitle, $newsContent, $newsCategoryId, $newsId));
//            return "Rekord zaktualizowany poprawnie";

        } catch (Exception $e) {
            return "Błąd podczas aktualizacji";
        }
        $pdo = Database::disconnect();
    }

}

?>


