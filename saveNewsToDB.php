<?php
include "News.php";
$title = $_POST['title'];
$content = $_POST['content'];
$categoryId = $_POST['category_id'];

/*function utf8ize($d)
{
    if (is_array($d))
        foreach ($d as $k => $v)
            $d[$k] = utf8ize($v);

    else if (is_object($d))
        foreach ($d as $k => $v)
            $d->$k = utf8ize($v);
    else
        return utf8_encode($d);
    return $d;
}*/

if ($title != null && $content != null && $categoryId != null) {
    try {
//        $newsID = News::createNews($title, $content, $categoryId);
        $newsID = News::createNews(htmlentities($title), htmlentities($content), htmlentities($categoryId));
        $lastInsertedNews = News::getOneNews($newsID);
        $categoryName = News::getCategoryName($lastInsertedNews['category_id']);
        $oneNews = ['id'=>html_entity_decode($lastInsertedNews['id']),'title' => html_entity_decode($lastInsertedNews['title']), 'content' => html_entity_decode($lastInsertedNews['content']), 'category' => html_entity_decode($categoryName['name'])];
//        $oneNews = ['id'=>$lastInsertedNews['id'],'title' => $lastInsertedNews['title'], 'content' => $lastInsertedNews['content'], 'category' => $categoryName['name']];
        echo json_encode($oneNews);
//        echo json_encode(utf8ize($oneNews));

    } catch (Exception $e) {
        echo "Wystąpił błąd podczas zapisywania obiektu do bazy danych";
    }
}
?>

